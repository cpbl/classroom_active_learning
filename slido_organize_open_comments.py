#!/usr/bin/python3
# This is a standalone module. You can copy it to wherever your downloaded Slido files are.
import pandas as pd
import numpy as np

df= pd.read_excel('Polls-per-user-ENVR201_McGill_Unintended_consequences_of_policy.xlsx')

dfo= df[['User Name','How are things going?']].fillna('')
dfo.columns=['name','howare']
dfo['howare'] = dfo['howare'].apply(lambda s: '; '.join(np.unique([ss.strip() for ss in s.split(';')])))
print(  '\n\n\n'.join((dfo.name+' '+dfo.howare).values ))

print('\n\n\n\n'+'='*1000+'\n\n\n')
print(  '\n'.join(dfo.howare.values[::-1] ))

from wordcloud import WordCloud  #pip3 install --user wordcloud
import matplotlib.pyplot as plt
wordcloud = WordCloud(max_font_size=300, width=2000, height=1000).generate('.\n'.join(dfo.howare.values))
plt.imshow(wordcloud)
plt.axis("off")
plt.savefig('wordcloud-tmp-needstrimming.png', dpi=300)
os.system('convert -trim wordcloud-tmp-needstrimming.png  wordcloud-tmp.png')

