#!/usr/bin/python
"""
Usage:
  perusall classlist [options]
  perusall grades [options]

Options:
  -h --help  get help


grades: On Perusall, click Gradebook and look fort he Download icon at the top of the page. Save to "perusall-gradebook.csv" (and also make a copy with today's date).   Also, click on assignments and download the list of them (The problem is, Perusall doesn't give dates for the assignments!)
  - weight each DAY's assignments' mean equally, not each separate reading. 
  - to do: i drop duplicate student IDs, but should also report them

classlist:
  - Download the classlist from minerva (!?)


 Maggie from Perusall, wrote:

Nov 8, 2021, 3:57 PM CST
Hey Chris!

We don't have this in the gradebook itself right now, but we suggest you could download the assignment info from the Assignments tab (Download > Assignment info). Alternatively, the gradebook is sorted by deadline, so you could take all of the columns to the left of the current assignment. Or, use the fact that that the gradebook average by default only includes assignments past their final deadline.

All the best,
Maggie


"""
import docopt
import pandas as pd
import numpy as np

LATEST_PERUSALL_GRADEBOOK_CSV = 'perusall-gradebook-20211221.csv'
# After 2021-12: assigment info not needed . Date is concatenated to assignment names in gradebook now.
#LATEST_PERUSALL_ASSIGNMENT_INFO = 'perusall-assignment-information-20211221.csv'
arguments = docopt.docopt(__doc__)

if arguments['classlist']:
    pdf = pd.read_table(LATEST_PERUSALL_GRADEBOOK_CSV, sep=',').set_index('Student ID')

    mdf = pd.read_table('classlist.csv', sep=',', encoding='iso-8859-1', skiprows=8).set_index('Student Name') # A bug 

    extraIDs =  set(pdf.index.to_list())-set(mdf.index.astype(str).to_list())
    missingIDs =  set(mdf.index.astype(str).to_list()) - set(pdf.index.to_list())


    print(extraIDs)

    print(pdf.loc[extraIDs])
    print(pdf.loc[extraIDs,['Name','Email']])

    print('missing students: \n', '\n  '.join( missingIDs))
    #for anid in missingID
    #print('missing students: \n', '\n  '.join( missingIDs))


    
if arguments['grades']:
    ungraded_videos = [
       #       'Topics preview for Prof Barrington-Leigh: What you should un-learn this term!',
       #       'Longman 2004 -- The Global Baby Bust',
       #       'Commoner 1975 -- How Poverty Breeds Overpopulation (and not the other way around)',
       #       'Dasgupta 1995 -- Population, poverty and the local environment',
       #       'Ehrlich and Ehrlich 2009 -- The Population Bomb Revisited',
       #       'Tragedy of the Commons',
       #       'Collective Action Problems (segment 1 of lecture)',
       #       'Collective Action Problems (segment 2 of lecture)',
       #       'Collective Action Problems (segment 3 of lecture)',
       #       '(USE THIS VERSION!) McGinn 1988: version with fonts, Do not use this version; it is a raster scan (McGinn 1998 -- Promoting Sustainable Fisheries). Read other version instead.',
       #       'Brown 2006 -- Feeding Seven Billion Well',
       #       'Glaeser Triumph of the City (excerpt)', 'Glaeser Transcript',
       #       'Wachsmuth Nature', 'The Highway and the City Mumford',
       #       'Pucher 1998 -- Urban Travel Behavior as the Outcome of Public Policy -o',
       #       'Values and intro to valuation (23 mins)',
       #       'Ecosystem Services (18 mins)', 'Valuation methods (53 mins)',
       #       'Valuation of Ecosystem Services: Intro to Valuation (8 mins) - Full video',
       #       'Valuation of Ecosystem Services: Classes of Values (7 mins) - Full video',
       #       'Monbiot, 2014. “Put a price on nature? We must stop this neoliberal road to ruin.”',
       #       'Marris, 2009. “Putting a price on Nature”.',
        #'Policies and options (49 mins)',
       #'New guide and province-wide challenge encourage sustainable travel at McGill - McGill Reporter - Entire web page, Sustainable Travel | Sustainability - McGill University - Entire web page, #BecauseIDidntFly | Sustainability - McGill University - Entire web page',
        'Aggregation and accounting across time: discounting (55 mins) - Full video',
              'More aggregation problem: uncertainty about the future (29 mins) - Full video',
              'Intro to Cost-Benefit Analysis (5 mins) - Full video',
              'Intro to Cost-Benefit Analysis (5 mins) - Full video.1',
       #'Goulder and Kennedy, 2011. “Interpreting and estimating the value of ecosystem services” (pages 15-27, i.e. first 13 pages)',
       #'Aggregation problem example',
              'Rebound effects, green paradoxes, and unintended consequences of policy (33 mins) - Full video',
       #'David Owen, “The Efficiency Dilemma: If our machines use less energy, will we just use them more?” The New Yorker, December 2010.',
              'ENVR201 Climate Policy I: Intro and context - Full video',
              'ENVR201 Climate Policy II: abatement cost curve and SCC - Full video',
       #'The Social Cost of Carbon | Climate Change | US EPA - Entire web page',
       #'The-EPA-is-rewriting-the-most-important-number-in-climate-economics',
              'ENVR201 Climate Policy III: carbon pricing (setup) - Full video',
              "David Keith's TED video on geoengineering: Please see my Instructions for a link to a better quality version",
        #'YaleEnvironment_2009_PuttingaPriceofCarbon_05_07_09'

        'ENVR201 Climate Policy IV: carbon pricing conclusion - Full video',
       'ENVR201 Climate Policy V: memorize these - Full video',
       'ENVR201 Climate Policy VI: Canadian and international policy - Full video',
       'ENVR201 Climate Policy VII: wrap up - Full video',

        #'“How to drive fossil fuels out of the US economy, quickly”, David Roberts, Vox, 2020',
       #'Solow An Economists Perspective',
       'ENVR201 module4 start and Solow - Full video',
       #'Guha A Third World Critique',
       #'Agyeman 2008 Toward a just sustainability',
       'ENVR201 wilderness Guha justice - Full video',
       #'Holdren 2008 -- S and T for Sustainable Well-being',
       #'Jacobson and Delucchi 2009 -- A Path to Sustainable Energy by 2030',
       'ENVR201 happiness lesson - Full video',
       'ENVR201 bright and green entire lesson - Full video',
        #'Barrington-Leigh-GTI2017-Sustainability-and-Well-Being'
        ]

    mdf = pd.read_table(LATEST_PERUSALL_GRADEBOOK_CSV, sep=',',).set_index(['Student ID', 'Last name', 'First name', 'Email', 'Average score',])
    
    # Extract due dates, and non-juxtaposed names:
    # Annoyingly, the length of the date string is not uniform, due to day of months being sometimes 1 and sometimes 2 digits long. So no fixed-width string approach possible. ie, this would fail:     assert all([c[-32:-28 ]=='Due ' for c in mdf.columns])
    def split_title_and_date(s):
        t,d = s.split('Due ') # Untrapped error if there's more than one occurrence of "Due "
        if d.endswith('.1'): d=d[:-3]
        return s,t,pd.to_datetime(d)

    zipped=mdf.columns.map(split_title_and_date)
    # Replace columns:
    mdf = mdf.rename(columns=dict([(td[0],td[1]) for td in zipped]))
    duedates = pd.DataFrame([(td[1],td[2]) for td in zipped], columns=['assignment','duedate'])
    

    
    # Now, group assignments (columns) by date. 
    # Now get due dates:
    #dfdd = pd.read_table(LATEST_PERUSALL_ASSIGNMENT_INFO, sep=',')
    #a2date = dfdd.set_index('Assignment name')[['Submission deadline']].to_dict()['Submission deadline']
    a2date =  duedates.set_index('assignment').to_dict()['duedate']

    # Put date in multi-index column
    ids = mdf[[c for c in mdf.columns if c not in a2date]]
    pgrades = mdf[[c for c in mdf.columns if c in a2date.keys()  and c not in ungraded_videos]].fillna(0)
    pgrades.columns = pd.MultiIndex.from_tuples(    [(a2date.get(c,''),c) for c in pgrades.columns]  )
    print(  pgrades.groupby(level=0, axis=1).apply(lambda x: np.unique(x.dropna().values.flatten())))
    pgrades = pgrades.fillna(0).replace({1:.6, 2: .85, 3:1})#.set_index(['Name', 'Student ID'])
    
    dailygrades = pgrades.groupby(level=0, axis=1).mean()

    avgrades = dailygrades.apply(lambda row: row.sort_values()[2:].mean(),  axis=1)

    # Now handle duplicate IDs :(
    # Let's be crude:
    avgrades.sort_values(ascending=False, inplace=True)
    means = pd.DataFrame( avgrades[~avgrades.index.duplicated()]  ).reset_index()


    
    import matplotlib.pyplot as plt
    plt.close('all')

    #drop_columns = ['Last Name', 'First name', 'Student ID', 'Email', 'Average score',]
    
    # Now begin weird MyCourses formatting
    means['End-of-Line Indicator']='#'
    means = means.rename(columns={'Student ID':'OrgDefinedId', 0:'Perusall Points Grade'}) #'Approximate Interim Perusall Points Grade'})
    means = means.loc[means.OrgDefinedId.astype(str).map(str.isnumeric)]    
    means.to_csv('tmp_out_persuall_interim_grades.csv', sep=',', index=False)
    means[['OrgDefinedId','Perusall Points Grade','End-of-Line Indicator']].to_csv('tmp_out_persuall_interim_grades_mycoursesformat.csv', sep=',', index=False)

    
    import os
    print(means)
    means['Perusall Points Grade'].hist()
    plt.savefig('perusall.hist.pdf')
    os.system('evince perusall.hist.pdf&')
    
