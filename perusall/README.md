The start of some code to handle the impossibly difficult and confusing for students grading scheme of Perusall.com

If we get scores of 0,1,2,3 for each reading on a given day, for each student, we need to rescale these two a better/fairer curve,
[1;3D

group multiple scores from a particular day's assignment into a single score for the day/due date,

maybe drop each student's worst couple of days

and average for each student

Worse, you have to download and merge two separate files from Perusall to do this: one contains assignment names and grades, and the other assignment names and the due dates for each.

It also checks for repeat student IDs and for unknown student IDs






Again, the current functionality of the "grades" mode is:

parse the downloaded perusall grade book, which lists assignment names but not dates

parse the downloaded perusall assignments list, which lists assignment names and not dates

check all the student IDs entered in Perusall against our class list, to 
identify problematic (unrecognized) IDs. (I had to correct those in 
Perusall or contact students to fix them. There is nothing in Perusall or our learning system linkage
that ensure the IDs entered are correct or known.)

omit all the videos after a certain date (students decided comments in videos were just distracting)

rescale all grades from 0,1,2,3  to a 0-1 scale, as advertised to students. 

group all grades by date, and take the average perusall grade for each due date for each person

drop the lowest two grades (dates) for each student

deal with duplicate student IDs / accounts

take the average over due dates for each student


