#!/usr/bin/python3
"""
Usage:
 speedDatingSchedule -n NAMES
 speedDatingSchedule -f filename
 speedDatingSchedule -c filename
 speedDatingSchedule test
 speedDatingSchedule -h

Options: 
  -h --help       show this help message
  -n NAMES        Comma separated list of names
  -f filename     filename with list of names (one per line)
  -c classfile    Use a (McGill) class list file, read by classroomActiveLearning

Pairwise unisex Speed Dating Schedule / Speed date scheduling
Produces PDF, HTML, and LaTeX versions of several matching rounds.

e.g. 
run speedDatingSchedule.py -n "Emma,Olivier,Poojan,Rohan,Samanth"
run speedDatingSchedule.py -c "/home/meuser/courses/430/classlist.tsv"


This seems to be a simple "round robin" scheduling problem, solved even on Wikipedia.
The algorithm is similar for even and odd numbers; with odd, just add an extra "nobody" person.

Consider N people who want to meet in pairs, e.g. 5 minutes each.
We will do R rounds. In general, 

   N must be even, so no one has to sit out.

and

   R <= N-1

since in N-1 rounds, everyone can meet everyone.



2023: If you have snap packages for your web browser, the launches to browser of local files won't work (!).
"""
from docopt import docopt


def rotate(l,n):  # Postive N rotates to the right: abcd --> dabc
    return l[-n:] + l[:-n]

def test_all():
    print(schedule_speed_dates(['a','b','c','d']))
    print(schedule_speed_dates(3))
    print(schedule_speed_dates(2))

    display_speed_dates(schedule_speed_dates(2))
    
def schedule_speed_dates(N,R=None):
    """
    Stand one person fixed at head of long, narrow table. Rotate everyone else around all the other positions. People play with the person facing them.

    N can be an integer, or a list of names (we'll call its length N)
    R can be less than or equal to N-1
    """
    if isinstance(N,int): 
        Names=[str(nn) for nn in range(1,N+1)]
    else:
        Names=N
        N=len(Names)

    if not  N%2==0:
        Names+=['nobody (bye)']
        N+=1

    import random
    random.shuffle(Names)

    if R is None: R=N-1

    # Initialize a dict containing each player's schedule
    schedule=dict([[nn,[]] for nn in Names])

    restOfTable=Names[1:]
    tableLength= int(N/2) -1
    for rn in range(R):
        lon=rotate(restOfTable,rn)
        lineups= list(zip(   Names[:1] + lon[:tableLength]    , [lon[tableLength]] + lon[tableLength:][::-1]  ))
        for apair in lineups:
            schedule[apair[0]]+=[apair[1]]
            schedule[apair[1]]+=[apair[0]]
    for nn,mm in list(schedule.items()): # This order is not well determined. It comes from dict. Sort?
        print((nn+':  '+str(mm)))
    return(schedule)

def display_speed_dates(schedule):
    import tempfile
    import os
    #fileTemp = 
    kk=[kkk for kkk in list(schedule.keys()) if 'nobody' not in kkk]
    outs= """ <HTML><body>
<TABLE border=1><TR><TD>Round</TD> """+' '.join(['<td><b>'+aplayer+'</b></td>' for aplayer in kk])+""" </TR>
"""+'\n'.join(['<TR '+(iround%2)*'BGCOLOR="#CCCC99"'+'><TD><b>%d</b>'%(1+iround)+'</TD> '+' '.join(['<td>'+schedule[aplayer][iround]+'</td>' for aplayer in kk])
               for iround in range(len(schedule[kk[0]]))])+"""
"""
    
    tex= r""" 

\documentclass[11pt]{standalone}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage[table]{xcolor}    % loads also colortbl

\begin{document}
  \rowcolors{2}{gray!25}{white}
  \begin{tabular}{|l""" + len(schedule.keys())*'|c'+ r"""|}
    \rowcolor{gray!50}
Round &  """+' &  '.join([r' {\bfseries '+aplayer+'} ' for aplayer in kk])+r""" \\ \hline 
"""+ '\n'.join(['{}&'.format(iround+1) + ' & '.join([schedule[aplayer][iround] for aplayer in kk])+ r' \\ \hline '
               for iround in range(len(schedule[kk[0]]))])+ r"""
  \end{tabular}
\end{document}
"""

    
    with tempfile.NamedTemporaryFile(delete = False, suffix='.html') as fileTemp:
        with open(fileTemp.name, 'wt') as fd:
            fd.write(outs)
        os.system('xdg-open '+fileTemp.name)
        # All of my attempts in 2023 to open anyting in firefox fail because firefox is from a snap package. It's sandboxed! You have to do a weird workaround. Why doens't xdg-open know about it??
        print(f' Not working: xdg-open {fileTemp.name}')
        #pip3 install -U pdfkit
        #pip3 install -U wkhtmltopdf
        if 0: 
            import pdfkit
            pdffile = fileTemp.name.replace('.html','')+'.pdf'
            pdfkit.from_file(fileTemp.name, pdffile)
        else:
            import weasyprint
            pdffile = fileTemp.name.replace('.html','')+'.pdf'
            weasyprint.HTML(fileTemp.name).write_pdf(pdffile)
        os.system('pdfcrop {f} {f}'.format(f=pdffile))
        os.system('xdg-open '+ pdffile)

        texfile=fileTemp.name.replace('.html','')+'tex.tex'
        with open(texfile, 'w') as fout:
            fout.write(tex)
        os.system('latexmk -output-directory={} '.format(os.path.split(texfile)[0]) +texfile)
        os.system('xdg-open '+ texfile.replace('.tex','.pdf'))
        ost= f"chromium-browser  {texfile.replace('.tex','.pdf')} &"
        print(ost)
        os.system(ost)
    print('\n\n')
    for nn,mm in list(schedule.items()): # This order is not well determined. It comes from dict. Sort?
        print((nn+':  '+str(mm)))
    print('\n\n')
    print(tex)
    
if __name__ == "__main__":
    # Do not put actual names into something that will go into the repo. Instead, copy the code below into a local file
    args = docopt(__doc__)
    assert not args['-f'] or not args['-n']
    names=None
    if args['test']:
        test_all()
        done_tests
    if args['-n']:
        names=args['-n'].split(',')
    elif args['-f']:
        names = [L.strip() for L in open(args['-f'], 'rt').readlines() if L.strip()]
    elif args['-c']:
        # This package may not be set up properly yet, so this import should be done locally
        from classroomActiveLearning import cpblClassroomTools
        ct=cpblClassroomTools(classlistfile=args['-c'])
        names = ct.classlist.uniqueFN.to_list()
    if names:
        datesched=schedule_speed_dates(names)
        display_speed_dates(datesched)    
        if 0:
            [L for L in """
    sample name1
    sample name2
    sample name3
        """.split('\n') if L.strip().strip(',') and not L.strip().startswith('#')]
        
