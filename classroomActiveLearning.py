#!/usr/bin/python3
"""
Usage:
  classroomActiveLearning  [options]
  classroomActiveLearning -h | --help
  classroomActiveLearning choose_student [options]
  classroomActiveLearning mark_absent [options]
  classroomActiveLearning record_score --score=<score> [options]
  classroomActiveLearning assign_groups_by_size [--groupsize=<groupsize>] [options]
  classroomActiveLearning assign_into_groups [--ngroups=<ngroups>] [options]
  classroomActiveLearning report_grades [options]
  classroomActiveLearning list-first-names [options]
  classroomActiveLearning emails
  classroomActiveLearning random-photo-list [options]

Options:
  -l --classlistfile=<classlistfile>   Specify class list 
  -h --help      Show this screen.
  -s --groupsize=<groupsize>     Group size
  -n --ngroups=<ngroups>        Number of groups
  -a --avoid-previous-n=<avoidn>  Do not pick on the  <avoidn> most recently picked students 


# N.B.  You do not actually write the "=" in the above syntax. So how to write it above?


list-first-names : useful for speedDating code. Uses first names and initials only where needed.

classroomActiveLearning emails: Simply report a list of student email addresses
classroomActiveLearning random-photo-list: Make a PDF listing students and their photos in random order, for a printable grading sheet of in-class responses. (I could also allow repeats in this, to make it more like the choose_student sampling process).

To back-create attendance dates, use :
  from lassroomActiveLearning import generate_fake_grade_entries
  generate_fake_grade_entries(['sep 10 2019', 'sep 12 2019'])  




Several functions. See -h output.

Assign the 1-student function and the submit-grades functions to a hotkey for easy access during class.
Randomly choose a student from the class list, and pop-up their name using operating system Desktop notification.

On the size-N algorithm: I want each group to be at least size N, and hopefully not much larger. So no orphan/small groups. Now implemented, along with N groups of roughly equal size.

What formats of student names does it recognize?  (This the format given by McGill's MyCourses2 system, when you download a CSV classlist)
How to download: use Minerva. -> Claslist-> at bottom of screen: download.  This seems to give me a csv (because of some setting? choice?), though the encoding is not utf8. The code below knows how to read this file directly as is, somewhat robustly.  (((hm, I had to use  encoding='iso-8859-1' in 2021 in another context))
 - columns named firstName and lastName
 - one column called "Student Name" with content formatted "last name, first name"

To do:
- set up keystroke to indicate someone is absent today? [DONE]
- avoid repeating someone who was just called in the last few? [DONE]
- set up a parameter for random student name so that the last N graded ones are excluded.

N.B. I have hardcoded aspects of my teaching schedule and course numbers into chooseClassListFile. Edit this in the obvious way.  A better way (not used) would be to have cron tasks that set the course or course list.

"""
import docopt
import pandas as pd
import os
import numpy as np
#from random import shuffle # df.sample used instead

GRADES_FILE='/home/meuser/courses/activeLearningGrades.tsv'

import time # Wow. "%c" format isn't even consistent between python and ipython on my own system!!
DATETIMEFMT='%Y %b %d %a %j %H:%M:%S %Z'
now = time.strftime(DATETIMEFMT)  # time.strftime("%c")
import datetime
hour = datetime.datetime.now().hour
AVOID_PREVIOUS_N_STUDENTS=3

def chooseClassListFile(coursenumber=None):
    """
    Some custom specification of the class list default, if not given on command line
    Or, suggest a file if 
    """
    if isinstance(coursenumber, str) and os.path.exists(coursenumber):
        classlistfile=coursenumber
    elif coursenumber is not None:
        classlistfile = '/home/meuser/courses/'+str(coursenumber)+'/classlist.csv'
        assert os.path.exists(classlistfile)
    #elif "Tue" in now and hour < 12:
    #    classlistfile='/home/meuser/courses/ENVR615/classlist.csv'
    #    assert os.path.exists(classlistfile)        
    elif ("Tue" in now or 'Thu' in now):# and hour > 12:
        #classlistfile='/home/meuser/courses/swb/classlist.csv'
        classlistfile='/home/meuser/courses/301/classlist.csv'
        assert os.path.exists(classlistfile)        
    elif ("Mon" in now or 'Wed' in now):# and hour >= 10:
        classlistfile='/home/meuser/courses/201/classlist.csv'
    #elif "Tue" in now or "Thu" in now:
    #    classlistfile='/home/meuser/courses/201/classlist.csv'
        assert os.path.exists(classlistfile)
    else:
        classlistfile='/home/meuser/courses/301/classlist.csv'
        assert os.path.exists(classlistfile)
    print(classlistfile)
    
    assert classlistfile.endswith('.csv')
    # Strangely, in 2019-12 it seems following is not being used:
    GRADES_FILE = classlistfile.replace('.csv', '-activeLearningGrades.tsv')
    if not os.path.exists(GRADES_FILE):
        with open(GRADES_FILE,'at') as ff:
            ff.write('Date	classfile	studentName	studentID	grade	grade2	grade3	grade4	grade5	grade6\n')
            ff.write('dummyDate	dummyclassfile	dummystudentName	dummystudentID	dummygrade	dummygrade2	dummygrade3	dummygrade4	dummygrade5	dummygrade6\n')
    return(classlistfile)

def recordGradeForLastStudent(thegrade):
    with open(GRADES_FILE,'at') as ff:
        ff.write('\t'+str(thegrade))
    os.system(' play /usr/share/sounds/KDE-K3B-Finish-Success.ogg &')
    # Close the (zenity) window  (make sure wmctrl is installed) showing the student name
    os.system("wmctrl -F -c 'ActiveLearning:1student'")
def  markLastStudentAbsent():
    recordGradeForLastStudent('A')
    # Close the (zenity) window  (make sure wmctrl is installed) showing the student name
    os.system("wmctrl -F -c 'ActiveLearning:1student'")

def loadGradeLogFromToday(): # Return a dataframe with students called today, in order.
    df=pd.read_csv(GRADES_FILE, sep='\t')#, dialect=None, compression=None, doublequote=True, escapechar=None, quotechar='"', quoting=0, skipinitialspace=False, lineterminator=None, header='infer', index_col=None, names=None, prefix=None, skiprows=None, skipfooter=None, skip_footer=0, na_values=None, na_fvalues=None, true_values=None, false_values=None, delimiter=None, converters=None, dtype=None, usecols=None, engine='c', delim_whitespace=False, as_recarray=False, na_filter=True, compact_ints=False, use_unsigned=False, low_memory=True, buffer_lines=None, warn_bad_lines=True, error_bad_lines=True, keep_default_na=True, thousands=None, comment=None, decimal='.', parse_dates=False, keep_date_col=False, dayfirst=False, date_parser=None, memory_map=False, nrows=None, iterator=False, chunksize=None, verbose=False, encoding=None, squeeze=False, mangle_dupe_cols=True, tupleize_cols=False, infer_datetime_format=False)
    todays=df[df.Date.map(lambda ss: isinstance(ss,str) and ss[:11]==now[:11])]
    return(todays)
    

def nChunks(l, n): # From SO, modified. FAILS!! e.g. l=22, n=6: bad allocation.
    """ Yield n successive chunks from l.
    Works for lists,  pandas dataframes, etc

    list length N. into n groups.
    minimum size is floorsize=floor(N/n)
    average surplus needed on remaining groups: avgsurplus=(N-n*floorsize)/(n-n0)
    to assign to this group: ceil(avgsurplus)
    Use recursion!? Not sure how, with yield

    2014 Sept: S.O. version nchunks or whatever is crap! I have written my own below.
    """
    import math
    n = int(n)
    remaining=len(l)
    floorsize=int(remaining/n) # int=floor?
    for ig in range(0,n-1):
        surplus=int(math.ceil(  (remaining-(n-ig)*floorsize )*1.0/(n-ig) ))
        #print(remaining,floorsize,surplus,ig)
        ifrom,ito=len(l)-remaining , len(l)-remaining +floorsize+surplus
        remaining=remaining-floorsize-surplus
        yield l[ifrom:ito]
    yield l[len(l)-remaining:]
    if 0:
        fooey
        for i in range(0, n-1):
            newn = int(1.0 * (len(l)-sofar) / n + 0.5)
            sofar+=newn
            yield l[i*newn:i*newn+newn]
        yield l[n*newn-newn:]

        sofar=0
        for i in range(0, n-1):
            newn = int(1.0 * (len(l)-sofar) / n + 0.5)
            sofar+=newn
            yield l[i*newn:i*newn+newn]
        yield l[n*newn-newn:]
def chunksOfSizeN(l,N):
    """
    To instead yield nearly-equal sized chunks of size <=N, use nChunks(
    """
    return(nChunks(l,   N/l)) # floor(N/l) in python 3?

def report_all_grades(classfile=None,maxOneZeroPerDay=True,allowOneDayAway=True): # Return a dataframe with all in-class students' records
    dfr=pd.read_csv(GRADES_FILE, sep='\t')#, dialect=None, compression=None, doublequote=True, escapechar=None, quotechar='"', quoting=0, skipinitialspace=False, lineterminator=None, header='infer', index_col=None, names=None, prefix=None, skiprows=None, skipfooter=None, skip_footer=0, na_values=None, na_fvalues=None, true_values=None, false_values=None, delimiter=None, converters=None, dtype=None, usecols=None, engine='c', delim_whitespace=False, as_recarray=False, na_filter=True, compact_ints=False, use_unsigned=False, low_memory=True, buffer_lines=None, warn_bad_lines=True, error_bad_lines=True, keep_default_na=True, thousands=None, comment=None, decimal='.', parse_dates=False, keep_date_col=False, dayfirst=False, date_parser=None, memory_map=False, nrows=None, iterator=False, chunksize=None, verbose=False, encoding=None, squeeze=False, mangle_dupe_cols=True, tupleize_cols=False, infer_datetime_format=False)


    print(len(dfr))
    df=dfr.dropna(subset=['grade'])
    print(len(df))
    df=df[-(df.grade.isin(['dummygrade']))]
    print(len(df))
    if 0: 
        df=df[df.Date.str.endswith('T')] # This is a kludge because CPBL old code started with a different format
        print(len(df))
    # How many "pass"s
    #df.grade=df.grade.replace({'A':'0'}).map(int)
    #df=df[-(df.grade.isin(['pass']))]
    # Wow, there's a crazy bug dealing with time zones (2019-12). Fix below: Kludge EDT to EST. Bug not yet reported.
    df.Date = df.Date.apply(lambda t: t if not t.endswith(' EDT') else t[:-4]+' EST')
    df['datet']=pd.to_datetime(df.Date,format=DATETIMEFMT)
    df['day']=df.datet.map(lambda x: x.strftime('%Y-%m-%d %b %d %a'))
    df.studentName= df.studentName.map(lambda ss:ss.strip())
    #print(df)
    dropstudents=['260416238','260553281','260350985']
    df=df[-(df.studentID.isin(dropstudents))]

    if maxOneZeroPerDay:
        dfzeros=df.query('grade == "0"').drop_duplicates(subset=['studentID','day','grade'])
        dfnonzeros=df.query('grade != "0"')
        df=pd.concat([dfzeros,dfnonzeros])
    print(len(df)) # But that allows for "A" and 0 in the same day.
          
    byStudent=df.groupby(['studentName','studentID','grade'])['grade'].count().unstack('grade').fillna(0)
    byStudent['N']=  df.groupby(['studentName','studentID',])['grade'].count()
    byStudent['average']=  df.groupby(['studentName','studentID',])['grade'].apply(lambda adf: adf.replace({'0':0, '1':1, '2':2, '3':3, 'A':0, 'pass':np.nan}).mean()/3)
    byStudent['averageAfterAllowance']=  df.groupby(['studentName','studentID',])['grade'].apply(lambda adf: np.nan if len(adf)<2 else adf.replace({'0':0, '1':1, '2':2, '3':3, 'A':0, 'pass':np.nan}).sort_values()[1:].mean()/3)
    if 0: 
        afterAllowance=[]
        for astudent,adf in df.sort_values('Date').groupby(['studentName','studentID']):
            print(adf[['studentName','grade','Date','studentID']])
        """    # You can only have one zero per day.
            for theday,oneday in adf.groupby('Date'):
                oneday.sort_values('grade',inplace=True)
                while len(oneday[oneday.grade==0])>1:
                    oneday.drop[0]
        """
    if 0:
        for astudent,adf in df.groupby(['studentName','studentID']):
            ff=adf.set_index(['studentName','studentID'])['grade']
            print((ff.count()))

    for course_student, adf in df.groupby(['classfile', 'studentName']):
        print(course_student, adf)

    print(byStudent.sort_values('averageAfterAllowance'))
    print(byStudent.sort_values('averageAfterAllowance')[['averageAfterAllowance','N']].reset_index())
    byStudent.sort_values('averageAfterAllowance').reset_index().to_csv(GRADES_FILE+'-participation-totals.tsv', sep='\t')
    print(' Wrote the above table to {} .'.format(GRADES_FILE+'-participation-totals.tsv'))
    return(byStudent)


    for courseday, adf in df.groupby(['classfile','day']):
        acourse,aday=courseday
        if classfile is not None and classfile is not acourse: continue
        print(acourse)
        print((str(aday)+'\t%f\t%d'%(adf.grade.averageAfterAllowance(),adf.grade.count())))
    return()#no_output_yet_except_printed)

def generate_fake_grade_entries(listofdates, classlistfile=None):
    """ Print some lines for grades file, for a given set of dates, one for each student per dayfirst.
Use format ['sep 10 2019', 'sep 12 2019']  for listofdates
"""
    ct=cpblClassroomTools(classlistfile=classlistfile)
    with open(GRADES_FILE,'at') as ff:
        for adate in pd.to_datetime(listofdates):
            ff.write('\n\n')
            for ii,onerow in ct.classlist.iterrows():
                outs='\t'.join([adate.strftime(DATETIMEFMT), ct.classlistfile, onerow['studentName'], onerow['ID']])
                ff.write(outs+'\n')
                print('WROTE TO LOG: '+outs)
        """
['nov 27 2019', 'nov 25 2019', 'sep 18 2019', 'nov 18 2019', 'nov 20 2019', 'nov 13 2019', 'nov 11 2019', 'nov 4 2019', 'oct 23 2019', 'oct 30 2019']
    """


def define_unique_first_names(df, inplace=False):
    """  This takes a DataFrame with unique 'ID' already as index, and columns firstName and lastName.

Logic:  In order to be unique: Use firstname OR firstname plus initial of last name OR first name and last name
      for each set of first names (size m):
         if m>1: 
            return blank, 
         else
            for each set of surname initials (size n):
                if n>1: return complete surnames else return initials
    """
    if not inplace:
        df = df.copy()
    ##df=self.classlist.set_index('ID',drop=False)#firstName')        
    for ii,adf in df.groupby(df.firstName):
        if len(adf)==1:
            df.loc[adf.ID,'suffix'] = ''
        else:
            for ii,odf in adf.set_index(adf.lastName.str[0]).groupby(level=0):
                if len(odf)==1:
                    df.loc[odf.ID,'suffix'] = ' '+ii
                else:
                    df.loc[odf.ID.values,'suffix'] = ' '+odf.lastName.values
    df['uniqueFN'] = df.firstName + df.suffix
    df['uFNhtml'] = df.firstName+r' <b> '+ df.suffix + r'</b>'
    df['uFNtex'] = df.firstName+r' {\bfseries '+ df.suffix + r'}'
    
    if not inplace:
        return df


def fileOlderThan(afile,bfiles,ageWarning=False):
    """ ie says whether the first fiel, afile, needs updating based on its parent, bfile.  ie iff afile does not exist or is older than bfile, this function returns true.

bfile can also be a list of files. in this case, the function returns true if any  of the bfiles is younger than the target file, afile; ie afile needs to be updated based on its antecedents, bfiles.

afile can also be a list of files... then the function returns true if any of the bfiles is younger than any of the afiles.

Rewritten, sept 2010, but features not yet complete. now afile,bfiles can be a filename, a list of filenames, or an mtime. And it's not vastly less inefficient than the first, recursive algorithm.
"""
    def oldestmtime(listoffiles): # Return oldest mtime of a list of filenames
        if any([not os.path.exists(afile) for afile in listoffiles]):
            return(-999999)
        return(min([os.path.getmtime(afile) for afile in listoffiles]))
    def newestmtime(listoffiles): # Return newst mtime of a list of filenames
        missingF=[afile  for afile in listoffiles if not os.path.exists(afile)]
        if missingF:
            print(('File assumed to exist is missing!: ',missingF))
        assert not missingF #  assert all([os.path.exists(afile) for afile in listoffiles])
        return(max([os.path.getmtime(afile) for afile in listoffiles]))

    # Ensure are lists
    if isinstance(afile,str):
        afile=[afile]
    if isinstance(bfiles,str):
        bfiles=[bfiles]

    # Compare ages:
    aa=afile
    bb=bfiles
    if isinstance(afile,str) or isinstance(afile,list):
        aa=oldestmtime(afile)
    if isinstance(bfiles,str) or isinstance(bfiles,list):
        bb=newestmtime(bfiles)
    # So now I hope aa and bb contain the mtimes, or -999999
    isOlder= aa<bb or aa<-99999

    # Check for existence of bfiles:
    gotReq=False
    def cpblRequireDummy(afile):
        return
    try:
        from cpblMake import cpblRequire # I could skip this if it doesn't exist, or....

        gotReq=True
    except:
        cpblRequire=cpblRequireDummy
        #print('   Failed to import cpblMake...')
        #pass
    for bf in bfiles:
        if gotReq:
            cpblRequire(bf)

    # Check for exists but getting aged:
    if 1:#####isOlder: # Don't bother with warnings if we know it's older (worse than aged)
        for af in afile:
            if os.path.exists(af) and os.path.getmtime(af) < time.time()-30*24*3600 and ageWarning:
                print("""     (N.B.: %s is older than a month. Consider recreating it?)"""%af)

    return( isOlder)# not os.path.exists(afile) or os.path.getmtime(afile)<os.path.getmtime(bfiles) )
    
###########################################################################################
###
class cpblClassroomTools():  #  # # # # #    MAJOR CLASS    # # # # #  #
    ###
    #######################################################################################
    """
    """
    # Caution: If you define variables here, before __init__, you can access them with cpblClassroomTools.avar , but those are common to the entire class, not to an instance.
    def __init__(self,classlistfile=None):

        if classlistfile is None or not os.path.exists(classlistfile):
            classlistfile=chooseClassListFile(classlistfile)
        self.classlistfile=classlistfile
        """
        Shuffling the classlist initially to make remaining routines simpler
        """
        pandasFilename = self.classlistfile.replace('.csv','')+'.pandas'
        if fileOlderThan(pandasFilename, self.classlistfile):
            df = self.loadClassList(self.classlistfile)
            df['ID'] = df.ID.map(str)
            df.set_index('ID', drop=False, inplace=True)
            define_unique_first_names(df, inplace=True)
            df.index= np.arange(len(df))
            df.to_pickle(pandasFilename)
        else:
            df = pd.read_pickle(pandasFilename)
        # Shuffle it.
        df=df.sample(frac=1)
        #ii=list(range(len(df)))
        #shuffle(ii)
        self.classlist=df
        self.writeEmailList()
        self.exemptions = self.loadClassList_exemptions(self.classlistfile)

    def loadClassList(self,clfile):
        # Shortcut if I've made a utf8 TSV file.
        tsvf=clfile[:-3]+'.tsv'
        if not os.path.exists(clfile) and clfile.endswith('.csv') and os.path.exists(tsvf):
            df=pd.read_csv(tsvf, sep='\t')
        else:

            # Clean up file a little before parsing
            import codecs
            LL=[LL.strip('\n') for LL in codecs.open(clfile,'r',encoding='iso-8859-1').readlines() if LL.strip('\n')]
            tmpfn='/home/meuser/tmp/tmpClasslistfile'
            startrow=[ii for ii in range(len(LL)) if 'Student Name' in LL[ii] or 'Email' in LL[ii]][0]
            with codecs.open(tmpfn,'w',encoding='iso-8859-1') as ff:
                ff.write('\n'.join(LL[startrow:])+'\n')

            # Load classlist (modified a bit)
            #df=pd.read_csv(tmpfn,skiprows=8,encoding='iso-8859-1',index_col=False)
            df=pd.read_csv(tmpfn,encoding='iso-8859-1',index_col=False)

        # Process first, last names
        if "Student Name" in df:
            df['firstName']=df['Student Name'].map(lambda ss: ss.split(',')[1].strip())
            df['lastName']=df['Student Name'].map(lambda ss: ss.split(',')[0].strip())
            df['SNtex']=df.apply(lambda dd: dd.firstName+r' {\bf '+dd.lastName+ r'}',axis=1)
            df['SNhtml']=df.apply(lambda dd: dd.firstName+r' <b> '+dd.lastName+ r'</b>',axis=1)
        df['studentName']=df['Student Name']
        return(df)
        
    def loadClassList_exemptions(self,clfile): #Loads students who don't want to be called on, ever
        return None if not os.path.exists(clfile+'-exempt') else  self.loadClassList(clfile+'-exempt')

    def print_list_of_first_names(self): # comma-separated list to stdout
        """
        Only include surname initial where needed
        """
        print( ','.join(self.classlist.uniqueFN))

        
        #vc = self.classlist.firstName.value_counts()
        #dups = vc[vc>1].index
        #names = self.classlist.firstName.values +(' '+self.classlist.lastName.values)* self.classlist.firstName.isin(dups)
        #return names.values

    def writeEmailList(self):
        if 'classlist.csv' in self.classlistfile:
            with open(self.classlistfile.replace('classlist.csv','classemails.txt'),'w') as ff:
                ff.write(' , '.join(self.classlist['Email'].values))

    def randomlyChooseOneStudent(self):
        """
        Pick a student randomly, but! avoid those marked as absent today, and those called in the previous AVOID_PREVIOUS_N_STUDENTS=3 calls.
        Also avoid anyone listed in the optional "exempt" file
        """
        todays=loadGradeLogFromToday()
        toAvoid=pd.concat([   todays[todays.grade.isin(['A'])], todays.iloc[::-1][:(AVOID_PREVIOUS_N_STUDENTS+1)]  ], sort=False)
        toAvoid['ID'] = toAvoid.studentID
        toAvoid = pd.concat([toAvoid, self.exemptions], sort=False)
        eligible=self.classlist[-self.classlist.ID.isin(toAvoid.ID)]
        dropped=self.classlist[self.classlist.ID.isin(toAvoid.ID)]
        print(('Avoided %d students as ineligible due to being absent, recently picked, or permanently exempt.'%(len(self.classlist)-len(eligible))))
        astudent=eligible.iloc[0]['uFNhtml']
        print((astudent+' cannot be in '))
        print((toAvoid.studentName.values))
        with open(GRADES_FILE,'at') as ff:
            ff.write('\n'+'\t'.join([now,self.classlistfile,self.classlist.iloc[0]['studentName'],self.classlist.iloc[0]['ID']]))
        os.system("""zenity --title "ActiveLearning:1student" --info --text "<span foreground='black' font='32'>%s</span>"   & """%astudent)
        # Also place the window in the external display location? 'g,x,y,w,h'
        os.system('sleep .3 && wmctrl -r ActiveLEarning:1student -R ActiveLearning:1student -e 0,2900,0,-1,-1 ')

    def randomlyAssignGroups(self,groupsize=False,numbergroups=False):
        """
        Split the class up either into roughly size groupsize or roughly into groupnumber of groups.

        Class size N. Just count of n=groupsize individuals from the shuffled list.  So we end up with leftover  1, 2, ...N-1.
        Distribute the extras to other groups?  unless it is preferred to have small groups

        How (sorry.. this needs a real algorithm)
        increment group name
        if more than N remain, assign N+x to next group, where x is 0 or 1, depending on whether remaining number mod N ==0

        For display, why not just create/show a PDF, rather than using a GUI text box tool? Seems easy enough, and it can be saved/ recalled.
        """
        # A default behaviour:

        if groupsize is False and numbergroups is False:
            groupsize=4
            numbergroups = None            
        elif groupsize is None and numbergroups is False:
            import subprocess
            output = subprocess.getoutput('/usr/bin/zenity --entry --title "Group size" --text "How many per group?" --entry-text "4"' )
            groupsize = int(output)
            numbergroups = None            
        elif groupsize is False and numbergroups is None:
            import subprocess
            output = subprocess.getoutput('/usr/bin/zenity --entry --title "Number of groups" --text "How many groups?" --entry-text "4"' )
            numbergroups = int(output)
            groupsize = None
        elif numbergroups is False:
            groupsize = int(groupsize)
        elif groupsize is False:
            numbergroups = int(numbergroups)

        #elif groupsize is '?' and numbergroups is None:
        #    nmaj = 10
        #    cmd = 'zenity --question --text="Are you {} years old ?"'.format(nmaj)
        #    subprocess.run(cmd, stdout = subprocess.PIPE)#shell=True)
        #    foooooo
        #assert groupsize is None or numbergroups is None
        df=self.classlist
        df['groupName']=''
        # Groups are named by letter
        groupnames='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        groupnames=list(groupnames) + [ii+jj for ii in groupnames for jj in groupnames]

        if numbergroups in [False,None]:
            groupsize,numbergroups = None,len(df)/groupsize


        assert numbergroups is not None
        # Number of groups (not size) is specified
        # Just grab the indices from the chunked dfs:

        for ii,adf in  enumerate(nChunks(df,numbergroups)):
            df['groupName'].iloc[adf.index]=groupnames[ii]
        df.sort_values('groupName', inplace=True)

        html=''
        tex=r"""\documentclass{article}\begin{document} """
        tex=r"""\documentclass{beamer}\usepackage[utf8]{inputenc}\begin{document}\begin{frame}[allowframebreaks]  """
        closing='\n'+r'\end{document}'+'\n'
        closing='\n'+r'\end{frame}\end{document}'+'\n'
        tex=r"""
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{color}
\usepackage{lscape}
\usepackage[landscape,margin=0cm]{geometry}
\begin{document} 
%\begin{landscape} 
\huge
"""
        closing='\n'+ r""" \end{document}""" +'\n'
        ngroups=0
        for gg, ss in df.groupby('groupName',sort=False)['uFNtex']:
            ngroups+=1
            html+=""" <table><tr><td>"""+gg+"""</td><td>"""+ss.values[0]+'</td></tr>'+  ''.join([ '<tr><td></td><td>'+nn+'</td></tr>' for nn in ss.values[1:]])+"""</table>"""
            tex+=r' \begin{tabular}{|rl|}\hline  {\bf\color{blue} '+gg+':} & '+ss.values[0]+r' \\'+' \n'+  ''.join([ ' & '+nn+r' \\ ' for nn in ss.values[1:]])+ r' \hline \end{tabular}'+' \n' + (ngroups%3==0)*r'\\ '

        print(html)
        tex+=closing
        import codecs
        DDR='/home/meuser/tmp/'
        with codecs.open(DDR+'tmpGroups.tex','w',encoding='utf8') as ff:
            ff.write(tex)
        os.system('cd '+DDR +' && pdflatex tmpGroups.tex')
        os.system('cd '+DDR +' && pdflatex tmpGroups.tex')
        os.system('evince  '+DDR+'tmpGroups.pdf &')
#        os.system("""zenity --title "" --text-info --html --text=" """+html+' "   &')
        print(("""zenity --title "" --text-info --html --text=" """+html+' " '))
        #"<span foreground='blue' font='32'>%s</span>" """%astudent)
        #os.system("""zenity --title "" --info --text " """+html+' " ') #<span foreground='blue' font='32'>%s</span>" """%astudent)
 


# Following is from cpblUtilities.configtools import get_docopt_mode
def get_docopt_mode(arguments, allow_none=False):
    """
    Calling method:
    arguments = docopt.docopt(__doc__)
    runmode = get_docopt_mode(arguments)
    """
    import inquirer

    knownmodes = [aa for aa in arguments if not aa.startswith('-')]
    runmode = ''.join([ss*arguments[ss] for ss in knownmodes])
    runmode = None if not runmode else runmode
    assert runmode in [None, 'default']+knownmodes
    if runmode is None and not allow_none:
        runmode = inquirer.prompt(
            [inquirer.List('A', message='Run which mode?', choices=[] + knownmodes)])['A']
    return(runmode)

        
if __name__ == '__main__':
    # Parse arguments, use file docstring as a parameter definition
    arguments = docopt.docopt(__doc__)
    if arguments['--avoid-previous-n'] is not None:
        AVOID_PREVIOUS_N_STUDENTS= int( arguments['--avoid-previous-n'])
    runmode = get_docopt_mode(arguments, allow_none=True)
    """
    import argparse

    parser = argparse.ArgumentParser(description='Various desktop pop-up tools for interactive classes.')
    parser.add_argument('-c','--classlist', #, metavar=None, type=str, nargs='1',
                        action='store',
                        help='A csv file containing data on the class list')
    parser.add_argument('--choose-student',#,  type=int, nargs='+',
                        action='store_true',
                       help=' Display one student name, and record the name for subsequent grading')
    parser.add_argument('-s', '--record-score', #-choose-student',#,  type=int, nargs='+',
                        action='store',
                       help=' Save a mark, associated with the recently displayed individual')
    parser.add_argument('-n', '--assign-groups-by-size', type=int, # nargs='+',
                        action='store',
                       help=' Assign students into groups of size n (or as close as possible)')
    parser.add_argument('-g', '--assign-into-groups', type=int, # nargs='+',
                        action='store',
                       help=' Assign students into G groups of roughly equal size')
    parser.add_argument('-a', '--mark-absent', 
                        action='store_true',
                       help=' Mark the most recently displayed individual as absent today')
    parser.add_argument('--report-grades', 
                        action='store_true',
                       help=' Produce reports for each course / each student on in-class assessment so far.')
    #parser.add_argument('--sum', dest='accumulate', action='store_const',
    #                   const=sum, default=max,
    #                   help='sum the integers (default: find the max)')

    args = parser.parse_args()
    """
    
    ct=cpblClassroomTools(classlistfile=arguments['--classlistfile'])
    
    if runmode == "choose_student":
        ct.randomlyChooseOneStudent()
    elif runmode == "mark_absent":
        markLastStudentAbsent()
    elif runmode == "record_score":
        recordGradeForLastStudent(arguments['--score'])
    elif runmode == "assign_groups_by_size":
        ct.randomlyAssignGroups(groupsize=arguments['--groupsize'])
    elif runmode == "assign_into_groups":
        ct.randomlyAssignGroups(numbergroups= arguments['--ngroups'])
    elif runmode == "report_grades":
        report_all_grades() # Return a dataframe with all in-class students' records
    elif runmode =='list-first-names':
        ct.print_list_of_first_names()
    elif runmode =='emails':
        print(','.join(ct.classlist.Email.to_list()))
    elif runmode == "random-photo-list":
        photofolder =  ct.classlistfile.replace('.csv', '-photos/')
        import glob
        df = pd.DataFrame({'fname': glob.glob(photofolder+'*.jpeg')}).sample(frac=1) # Randomize/shuffle order
        df['studentfn'] = df.fname.map(lambda s: os.path.splitext(os.path.split(s)[1])[0])


        DDR='/home/meuser/tmp/'
        with open(DDR+'tmpClassPhotos.tex','w',encoding='utf8') as ff:
            ff.write(r"""


\documentclass{article}
\usepackage[utf8]{inputenc}
%\usepackage{color}
%\usepackage{lscape}
\usepackage{graphicx}
\usepackage[margin=1cm]{geometry}

\usepackage{longtable}
\usepackage{multicol}
%\usepackage{supertabular}



\newsavebox\ltmcbox
\newenvironment{fakelongtable}
        {\setbox\ltmcbox\vbox\bgroup
        \csname @twocolumnfalse\endcsname
        \csname col@number\endcsname\csname @ne\endcsname}
      {\unskip\unpenalty\unpenalty\egroup\unvbox\ltmcbox}



      

\begin{document}
\twocolumn
%\begin{landscape} 
\huge
%\usepackage{array,longtable}
%\renewcommand*{\arraystretch}{1.5}
% \begin{supertabular}{rlr}
\begin{fakelongtable}
  
    \begin{longtable}{rlr}

"""+   ' \n '.join([r' \includegraphics[width=25mm]{{{}}} & {} \\ '.format(a,b)   for a,b in df.values]) + r"""

  % \end{supertabular}
  \end{longtable}
\end{fakelongtable}
\end{document}
""")
            

        os.system('cd '+DDR +' && pdflatex tmpClassPhotos.tex')
        os.system('cd '+DDR +' && pdflatex tmpClassPhotos.tex')
        os.system('evince  '+DDR+'tmpClassPhotos.pdf &')

        
    else: # Demo

        ct=cpblClassroomTools(classlistfile=arguments['--classlistfile'])
    #    ct.randomlyAssignGroups(3)
    #    ct.randomlyAssignGroups(4)
        ct.randomlyAssignGroups(10)
        #ct.randomlyChooseOneStudent()



