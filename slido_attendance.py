#!/usr/bin/python3
__author__='cpbl 2022'

# This is a standalone module. You can copy it to wherever your downloaded Slido files are.
"""
This is a pain because
 - people have more than one email address (@mcgill.ca and @mail.mcgill.ca) and
 - we may want to give partial credit for late attendane per day, etc.

"""

import pandas as pd
import os
import glob
import numpy as np


def sum_columns_by_email_address_stem_in_index(df):
    # How to handle duplicate addresses??

    df['UIDstem'] = df.index.str.split('@').str[0]
    # Check for double-attendances
    df2 = df.groupby('UIDstem').sum()
    print('Double attendances!: ',  df2.loc[(df2>1).any(axis=1)].T )
    if len(df2)<len(df):
        input(f' Note: We are grouping either for a single class or for overall scores by these email addresses: {df} {df2}.  Acknowledge?')

    # Remove column 'UIDstem'
    df = df.drop(columns='UIDstem')
    return df

#slidodir=''
slidodir = '/home/meuser/courses/201/slido/'
xlsxs=  glob.glob(f'{slidodir}Polls-per-user*.xlsx')

#id_key= 'User ID'
id_key= 'User Email'

dfs=pd.DataFrame()
for afile in xlsxs:
    df=pd.read_excel(afile).rename(columns={
        'Participant Email':'User Email', # Kevin gave me the wrong file type
        })
    #uids= sorted(df[id_key].dropna().unique() )

    colname = afile.replace(f'{slidodir}Polls-per-user-ENVR201_','').replace(f'{slidodir}Polls-per-user-ENVR201','').replace('.xlsx','')
    colname = os.path.split(colname)[1]
    assert 'Polls-per' not in colname
    # Deal differently with different formats. If this is the polls-per-user format (every answer), let's make sure that student responded to first question
    if len(df.columns)> 4:
        df = df.dropna(subset='User Email').set_index('User Email')
        firstCol = [c for c in df.columns if c not in ['User ID', 'User Name', 'User Email', 'User company',       'Total Correct Answers', 'Total Questions Answered']][0]

        adf=pd.DataFrame({colname: pd.notnull(df[firstCol])*2/3  + 1/3,
                           'UID':df.index})

    elif 'Joined at' in df.columns:
        df = df.set_index('User Email')
        arrived = pd.to_datetime(df['Joined at'])
        import datetime

        adf=pd.DataFrame({colname: (arrived < arrived.min()  + datetime.timedelta(minutes=5))* 2/3 + 1/3,
                           'UID':df.index})
    else:
        ohoh



    # Btw, let's check for weird email address conflicts
    mcgilladdresses= [s for s in adf.index if '@mcgill' in s]
    print(' Following addresses are not mail.mcgill: ',  mcgilladdresses)
    for ma in mcgilladdresses:
        print(adf.loc[[x for x in adf.index if ma.split('@')[0] in x]])
    #cdups = [s.replace('mcgill.ca', 'mail.mcgill.ca') for s in adf.index]

    # How to handle duplicate addresses??
    adf = sum_columns_by_email_address_stem_in_index(adf)


    if dfs.empty:
        dfs=adf
    else:
        dfs = adf.merge(dfs, how='outer') #+= [adf.set_index('UID')]
    print(afile, adf)
DF = dfs.set_index('UID').fillna(0)

#print(' By class: ',DF.sum())
#totals = DF.sum(axis=1)


# Whoops. My colleague downloaded the wrong files for some classes... Not sure what they are called.
xlsxs= glob.glob(f'{slidodir}????.xlsx')
if xlsxs: # For 2022?  Not sure what format this is
    #id_key= 'User ID'
    #id_key= 'Participant Email' #OMG in his files the column name is not even consistent.

    dfs= pd.DataFrame()
    for afile in xlsxs:
        df=pd.read_excel(afile)
        id_key ='Participant Email' if 'Participant Email' in df.columns else 'User Email'
        uids= sorted(df[id_key].dropna().unique() )
        colname = afile.replace(f'{slidodir}Polls-per-user-ENVR201_','').replace('.xlsx','').replace(slidodir, '')
        adf=pd.DataFrame({colname:np.ones(len(uids)), 'UID':uids})#.set_index('UID')

        if dfs.empty:
            dfs=adf
        else:
            dfs = adf.merge(dfs, how='outer')#+= [adf.set_index('UID')]
        wot

    DF2 = dfs.set_index('UID').fillna(0)

    DF = DF.join(DF2).fillna(0)




DF=sum_columns_by_email_address_stem_in_index(DF)

print(' By class: ',DF.sum())
# Drop One lowest attendance?
byclass =   DF.apply(lambda row: sum(sorted(row.values)[1:]),).sort_values().to_frame( name='Attendance')#
bystudent = DF.apply(lambda row: sum(sorted(row.values)[1:]), axis=1).sort_values().to_frame( name='Attendance')


print('2023 kludge: drop all with <=2 attendances')
bystudent = bystudent.query('Attendance>=2')


# Finalize output files/graphics


bystudent.to_csv('attendance_by_student.csv')
byclass.to_csv('attendance_by_class.csv')

import matplotlib.pylab as plt
plt.close('all')
bystudent.hist(bins=np.arange(20)+.5)
plt.xticks(range(20))
plt.savefig('histbystudent.pdf')

byclass.hist()#bins=np.arange(20)+.5)
import matplotlib.pylab as plt
plt.savefig('histbyclass.pdf')
