#!/usr/bin/python3
"""
Usage:
  cheating-detector-pairwise.py <csvfile> [options]
  cheating-detector-pairwise.py -h | --help

Options:
  -h --help      Show this screen.
  -v --visualize      Generate images of each match
  -m --mcgill-format   Assume input csv comes from MyCourses2, rather than UCSC's 
  -f --forceUpdate    Recreate pngs/pdfs?
  -c --coincidences    Analyze based on coincidences across multiple questions for each pair
  -t --threshold=<threshold>   Set threshold (not implemented?) [default: 0.6]
  -N --minimum-coincidences=<mc>   Set minimum number of questions required for suspicion [default: 3]
  --examine=<student1,student2,question_number>    Just launch a meld for this question and last name pair
  -V --verbose         Debugging
#
# Analyze exam scores for cheating, on a question by question basis
# Needs the csv file of exam questions downloaded from Canvas (UCSC) or with -m flag, from MyCourses2 (McGill).

With the -v flag, generate PDFs of meld-style text comparisons for each pair of questions


# e.g. 2020-12: merged two variants:
   cheating-detector-pairwise.py all-but-one-final-exam-responses.csv -mv --threshold=.6
   cheating-detector-pairwise.py all-but-one-final-exam-responses.csv -mv --threshold=.4 -c --minimum-coincidences=3

# e.g. 2021-03: You can also just use this as an interface to meld, ie to compare a specific pair and question number
run cheating-detector-pairwise.py all-but-one-final-exam-responses.csv -m --examine=student2,student1,7

"""
import docopt


from nltk import sent_tokenize
import os
import numpy as np
import pandas as pd
import sys
from sklearn.feature_extraction.text import TfidfVectorizer


OUTFN = 'similar_answers_Q'
IDCOL = 'email'
class copyDetector():
    def __init__(self, inFn):
        self.df = pd.read_csv(inFn)
        for col in self.df.columns:
            self.df[col] = self.df[col].fillna(' ')
        self.allpairs=[]#{} # Also look for coincidences: pairs of students sharing multiple answers
    def calcScores(self, column):
        """calculate similarity scores for a given column of the dataframe
        See https://stackoverflow.com/questions/8897593/how-to-compute-the-similarity-between-two-text-documents"""
        vect = TfidfVectorizer(min_df=1, stop_words="english")                                                                                                                                                                                                   
        tfidf = vect.fit_transform(self.df[column].values)                                                                                                                                                                                                                       
        self.pairwise_similarity = tfidf * tfidf.T 
        self.pairwise_similarity.setdiag(0)

    def get_most_similar(self, column, cutoff, verbose=True):
        self.calcScores(column)
        outFn = OUTFN+column+'.txt'
        if os.path.exists(outFn): os.remove(outFn)
        for ii, row in self.df.iterrows():
            if len(row[col])<10: 
                continue
            aboveCutoff = (self.pairwise_similarity[ii]>cutoff).indices
            otherAnswers = self.df.loc[aboveCutoff]
            if not otherAnswers.empty:
                outTxt = '\n\n*******\nSTUDENT: {}\nANSWER: {}\nOTHER ANSWERS:\n'.format(row[IDCOL],row[column])
                outTxt+= '\n\n'.join(otherAnswers[[IDCOL,column]].astype(str).apply('\n'.join, axis=1).values)
                if verbose:                 print(outTxt)
                with open(outFn, 'a') as fo:
                    fo.write(outTxt+'\n')
                for a,b in  [[row[IDCOL],ii] for ii in  otherAnswers[IDCOL].values]:
                    self.allpairs+=[ [a,b,column] ]
                    #self.allpairs+=[ [b,a,column] ]
                    #  self.addpair(self.allpairs, a,b,  .5)


    def report_pairs(self,sortby=None, cutoff=1):
        # A different approach, in which we look for coincidences across multiple questions (columns). Thus if two people cheat on a single question, we'll let them go with this method.
        df=pd.DataFrame(self.allpairs, columns=['a','b','Q'])
        listQs=df.groupby(['a','b']).Q.agg(Qs=lambda L: ','.join(L), N=np.size).query('N>={}'.format(cutoff))
        if sortby in [None, 'name']:
            listQs = listQs.sort_values(['a'])
        elif sortby in ['counts']:
            listQs = listQs.sort_values(['N'], ascending=False)
        return listQs


def meld_individual_response(student1, student2, qn):
    """ Pass two last names and the question number. 
    Doesn't yet work properly if there's more than one student with either name.

    This function relies on globally-defined stuff from main ! 
    """
    qn=int(qn)
    dfq = df3[[qn]]
    # Bring back in firstname, last name:

    dfq = dfq.join(  df[['FirstName','LastName','Username']].drop_duplicates().set_index('Username')  )
    dfq['FirstName']  = dfq['FirstName'].str.lower()
    dfq['LastName']  = dfq['LastName'].str.lower()
    dfq = dfq.set_index('LastName')
    #assert len(dfq.loc[student1]) ==1
    #assert len(dfq.loc[student2]) ==1
    fn1, fn2 =  student1+'-'+str(qn),    student2+'-'+str(qn)
    with open(student1+'-'+str(qn), 'wt') as fout:
        fout.write(dfq.loc[student1,qn]+'\n')
    with open(student2+'-'+str(qn), 'wt') as fout:
        fout.write(dfq.loc[student2,qn]+'\n')
    os.system('meld '+fn1+' '+fn2 +'  &')


if __name__ == '__main__':
    # Parse arguments, use file docstring as a parameter definition
    arguments = docopt.docopt(__doc__)
    inFn = arguments['<csvfile>']
    forceUpdate=arguments['--forceUpdate']
    coincidences = arguments['--coincidences']
    MIN_COINCIDENCES = arguments['--minimum-coincidences']
    TOLERANCE= float(arguments['--threshold']    )
    """
    inFn = 'myfilepath/Final exam Quiz Student Analysis Report.csv'  
    inFn = 'all-but-one-final-exam-responses.csv'
    """
    if arguments['--mcgill-format']: #not os.path.exists(inFn+'UCformat.csv'):
        df=pd.read_table(inFn, sep=',')
        if arguments['--verbose']: print('following users have ultiple attempts: ', df[df['Attempt #']>1].Username.unique())
        df2=df[['Username','Q #','Attempt #','Answer']]
        df2=df2.groupby(['Username','Q #']).last().reset_index().rename(columns={'Q #':'Q', 'Username':'email'}).rename(columns=dict([(i,str(i)) for i in range(11)]))
        #df2.columns=['email','Q','Answer']
        df3=df2.pivot(index=['email'], columns='Q', values='Answer')

        df3.to_csv(inFn+'UCformat.csv')
        
        inFn = inFn+'UCformat.csv' #'all-but-one-final-exam-responses.csvUCformat.csv'

    if arguments['--examine']:
        student1,student2,qn =arguments['--examine'].split(',')

        meld_individual_response(student1, student2, qn)
        """
        my_env = os.environ.copy()
        proc = subprocess.Popen(['meld',sa, sb], env=my_env)
                    print( "start process with pid {}".format(proc.pid))

        foiuwe
        """
        
    else:
        cd = copyDetector(inFn)

        for col in range(2,11):#['B1','B2', 'B3', 'B4', 'B5', 'B6','C1','C2','C3','C4']:
            cd.get_most_similar(str(col), TOLERANCE, verbose=False)
        n=cd.report_pairs()
        n.to_csv('offenders_by_name.csv', sep='\t')
        c=cd.report_pairs(sortby='counts', cutoff= MIN_COINCIDENCES)
        c.to_csv('top_offenders.csv', sep='\t')

    if arguments['--visualize']:
        # Now, let's produce some vizualizations:
        import subprocess
        import time
        P='___tmp45678723'
        df = cd.df.set_index('email')
        os.system('killall meld')
        c=c.reset_index()
        c['sa']=c.a.map(lambda s: s.replace('mail.mcgill.ca','').replace('@','-').replace('.','-'))
        c['sb']=c.b.map(lambda s: s.replace('mail.mcgill.ca','').replace('@','-').replace('.','-'))
        n=n.reset_index()
        n['sa']=n.a.map(lambda s: s.replace('mail.mcgill.ca','').replace('@','-').replace('.','-'))
        n['sb']=n.b.map(lambda s: s.replace('mail.mcgill.ca','').replace('@','-').replace('.','-'))

        allpdfs=[]
        donotskip=True
        png=True
        useDF = c if coincidences else n
        for ii,adf in useDF.reset_index().iterrows():
            for Q in adf['Qs'].split(','):
                sa,sb = '_'+adf['sa']+Q, '_'+adf['sb']+Q
                q1,q2 = df.loc[adf['a'],Q], df.loc[adf['b'],Q]
                fn= '_'+sa+'---'+sb+'.pdf'
                if '_'+sb+'---'+sa+'.pdf' in allpdfs: continue
                allpdfs+=[fn]
                with open(sa, 'wt') as fout:
                    fout.write(q1+'\n')
                with open(sb, 'wt') as fout:
                    fout.write(q2+'\n')
                if not os.path.exists(fn) or forceUpdate:
                    my_env = os.environ.copy()
                    proc = subprocess.Popen(['meld',sa, sb], env=my_env)
                    print( "start process with pid {}".format(proc.pid))
                    time.sleep(3)
                    ##fn='{}{}--Q{}.pdf'.format(a,b,Q).replace('@','_').replace(' ','_')
                    sh="""import -resize 800 -window `wmctrl -l | grep -i {} | awk '{{print $1}}'` {} """.format(sa, fn)
                    if png:
                        sh="""import             -window `wmctrl -l | grep -i {} | awk '{{print $1}}'` {} """.format(sa, fn[:-4]+'.png')
                    print(sh)
                    os.system(sh)
                    time.sleep(3)
                    if png:
                        os.system('convert {f}.png {f}.pdf'.format(f=fn[:-4]))
                        print(' Cropping '+fn)
                        os.system(' pdfcrop {} {} &'.format(fn,fn))
                    # kill if process didn't finish
                    if proc.poll() is None:
                        #print( "Killing process %s with pid %s " % (to_run,proc.pid))
                        proc.kill()
                    os.system('killall meld')
                    
                #os.system('meld '+P+'a '+P+"""b &""")
                #outs=subprocess.check_output(['meld',P+'a',P+'b', '&'])
                #oiu

        sh ='pdftk  {} cat  output allpdfs-{}.pdf'.format(' '.join(allpdfs),  'c' if coincidences else 'n')
        print(sh)
        os.system(sh)
