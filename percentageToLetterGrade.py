#!/usr/bin/python3
import numpy as np

def percentageToLetterGrade(percentage, roundUpPoints=1, letterBoundariesOnly=True):
    """ If roundUpPoints = 1, then anyone whose percentage grade is >=78.5 will be rounded up to 80.
Similar for F->D and D->C and C->B: ie on the letter boundaries, we upgrade marginal cases.

If letterBoundariesOnly is False, then we do the upgrades on every boundary.

    percentage is a scalar float


    
    
A	4.0	85 – 100%
A-	3.7	80 – 84%
B+	3.3	75 – 79%
B	3.0	70 – 74%
B-	2.7	65 – 69%
C+	2.3	60 – 64%
C	2.0	55 – 59%
D	1.0	50 – 54%
F (Fail)	0	0 – 59%

    """

    from typing import Iterable
    if isinstance(percentage, Iterable):
        return([percentageToLetterGrade(p, roundUpPoints=roundUpPoints, letterBoundariesOnly=letterBoundariesOnly) for p in percentage])
    
    
    thresholds =[
                  (85,'A'),
                  (80,'A-'),
                  (75,'B+'),
                  (70,'B'),
                  (65,'B-'),
                  (60,'C+'),
                  (55,'C'),
                  (50,'D'),
                  (0,'F'),]
    if letterBoundariesOnly is True:
        tb = [50, 55, 65, 80]
        thresholds = [(a-.5-roundUpPoints, b) if a in tb else (a-.5,b)        for a,b in thresholds]
    else:
        thresholds = [(a-.5-roundUpPoints, b) for a,b in thresholds]

    return [b for a,b in thresholds if percentage >=a ][0]   


if __name__=="__main__":
    for rp in [0,1,2]:
        for a in np.arange(45,84,.1):
            print('{}: {}->{}'.format(rp,a, percentageToLetterGrade(a, roundUpPoints=rp)))
        
    
