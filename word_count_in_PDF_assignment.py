#!/usr/bin/python3
import glob
import os
import re
from collections import Counter

with open('counts_out.tsv', 'wt') as fout:
    dataout=[]
    for fn in glob.glob('*.pdf'):
        if ' ' in fn:
            print(' use "nospaces -d" to remove spaces in filenames first.')
            continue
        os.system('pdftotext -layout {fn} {fn}.txt '.format(fn=fn))
        txt = open(fn+'.txt', 'rt').read()

        alts=4 # Number of alternatives, below.
        parts = re.split('(?i)(\n.?\s*references)|((?i)\n.?\s*bibliography)|((?i)\n.?Works Cited)|((?i)\n\s*articles)', txt)#, re.IGNORECASE))    
        print(' Found {} parts in {}'.format(len(parts), fn))
        lens = [0 if not isinstance(x,str) else  sum(Counter(x.lower().split()).values()) for x in parts]
        big = [(ii,L) for ii,L in enumerate(lens) if L>1000][0]
        print(' We think length is {}'.format(big[1]))

        if len(parts) == alts + 2:
            maintext = parts[0]
        elif len(parts) == 2*alts + 3:
            pass
        fout.write('\t'.join( [fn, str(big[1]), parts[big[0]].split('\n')[0], parts[big[0]].split('\n')[-1] ] ) + '\n')
        try:
            with open(fn+'-maintext.txt', 'wt') as fout2:
                fout2.write(parts[big[0]].replace('\n\n','\n'))
        except IndexError as e:
            wowo
            print(' FAILED TO FIND PARTS of {}'.format(fn))
        # 11 parts: there is a TOC. So take
    #    assert len(parts) ==4
        #counts =
    #    owieu

    # Counter(sentence.lower().split())    


